import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { ContainerComponent } from './main.app/container/container.component';
import { AppRoutingModule } from './app-routing.module';
import { Error404Component } from './error404/error404.component';
import { LandingComponent } from './landing/landing.component';
import { ManageusersComponent } from './main.app/manageusers/manageusers.component';
import { NavprimaryComponent } from './main.app/navprimary/navprimary.component';
import { HeaderprimaryComponent } from './main.app/headerprimary/headerprimary.component';
import { WorkspaceComponent } from './main.app/workspace/workspace.component';
import { OpenQuestionsComponent } from './main.app/navprimary/open-questions/open-questions.component';
import { ClosedQuestionsComponent } from './main.app/navprimary/closed-questions/closed-questions.component';
import { PostQuestionComponent } from './main.app/navprimary/post-question/post-question.component';
// Services

import { OptionsService } from './main.app/navprimary/options.service';
import { UserSettingsComponent } from './main.app/navprimary/user-settings/user-settings.component';
import { SiteSettingsComponent } from './main.app/navprimary/site-settings/site-settings.component';
import { PaymentsComponent } from './main.app/navprimary/payments/payments.component';


@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    Error404Component,
    LandingComponent,
    ManageusersComponent,
    NavprimaryComponent,
    HeaderprimaryComponent,
    WorkspaceComponent,
    OpenQuestionsComponent,
    ClosedQuestionsComponent,
    PostQuestionComponent,
    UserSettingsComponent,
    SiteSettingsComponent,
    PaymentsComponent
  ],
  imports: [
    BrowserModule , AppRoutingModule, HttpModule
  ],
  providers: [OptionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
