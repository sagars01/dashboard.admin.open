import { NgModule } from '@angular/core';
import { Routes , RouterModule } from '@angular/router';

// Component Imports
import { ContainerComponent } from './main.app/container/container.component';
import { LandingComponent } from './landing/landing.component';
import { Error404Component } from './error404/error404.component';
import { OpenQuestionsComponent } from './main.app/navprimary/open-questions/open-questions.component';
import { ClosedQuestionsComponent } from './main.app/navprimary/closed-questions/closed-questions.component';
import { PostQuestionComponent } from './main.app/navprimary/post-question/post-question.component';
import { ManageusersComponent } from './main.app/manageusers/manageusers.component';
import { SiteSettingsComponent } from './main.app/navprimary/site-settings/site-settings.component';
import { PaymentsComponent } from './main.app/navprimary/payments/payments.component';

const routes: Routes = [
  {
    path : '',
    component : LandingComponent
  },
   {
     path : 'dashboard',
     component : ContainerComponent,
     children : [
      {
          path: '',
          redirectTo: 'openquestions',
          pathMatch: 'full'
      },
        {
          path : 'openquestions',
          component : OpenQuestionsComponent
        },
        {
          path : 'closedquestions',
          component : ClosedQuestionsComponent
        },
        {
          path : 'postquestion',
          component : PostQuestionComponent
        },
        {
          path : 'usersettings',
          component : ManageusersComponent
        },
        {
          path : 'manage',
          component : SiteSettingsComponent
        },
        {
          path : 'payments',
          component : PaymentsComponent
        },
     ]
   },
  {
    path : '404',
    component : Error404Component
  },
  {
    path : '**',
    redirectTo : '404'
  },
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports : [RouterModule.forRoot(routes)],
  declarations: []
})

export class AppRoutingModule {
}
