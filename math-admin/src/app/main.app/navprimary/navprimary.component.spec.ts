import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavprimaryComponent } from './navprimary.component';

describe('NavprimaryComponent', () => {
  let component: NavprimaryComponent;
  let fixture: ComponentFixture<NavprimaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavprimaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavprimaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
