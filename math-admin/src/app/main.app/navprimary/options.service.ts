import { Injectable } from '@angular/core';

@Injectable()
export class OptionsService {

  constructor() {}
  getAllOptions() {
    let options: any;
    options = [
      {
        id : 1,
        name : 'Open Questions',
        component : 'app-open-questions',
        route : '/openquestions',
        icon : 'fa fa-question-circle'
      },
      {
        id : 2,
        name : 'Closed Questions',
        component : 'app-closed-questions',
        route : '/closedquestions',
        icon : 'fa fa-question'
      },
      {
        id : 3,
        name : 'Post a Question',
        component : 'app-post-questions',
        route : '/postquestion',
        icon : 'fa fa-gg'
      },
      {
        id : 4,
        name : 'User Settings',
        component : 'app-user-settings',
        route : '/usersettings',
        icon : 'fa fa-users'
      },
      {
        id : 5,
        name : 'Site Settings',
        component : 'app-site-settings',
        route : '/manage',
        icon : 'fa fa-cog'
      }
    ];
    return options;
  }

  getAllText() {
    let allText: any;
    allText = [
      {
        id : '1',
        name : 'dashboard_head',
        maintext : 'Math Foundation',
        subtext : 'Solving Maths',
        link : ''
      },
      {
        id : '2',
        name : 'sidebar_head',
        maintext : 'Main Navigation',
        subtext : 'Solving Maths',
        link : ''
      }
    ];
    return allText;
  }

  getAdvancedSettingsOptions() {
    let allOptions: any;
    allOptions = [
      {
        id : 1,
        name : 'Payments',
        component : 'app-payments',
        route : '/payments',
        icon : 'fa fa-credit-card'
      }
    ];
    return allOptions;
  }
}
