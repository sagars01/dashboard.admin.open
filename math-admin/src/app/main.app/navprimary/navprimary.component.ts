import { Component, OnInit } from '@angular/core';
import { OptionsService } from './options.service';

@Component({
  selector: 'app-navprimary',
  templateUrl: './navprimary.component.html',
  styleUrls: ['./navprimary.component.css']
})
export class NavprimaryComponent implements OnInit {
  navOptions: any;
  componentText: any;
  advancedOptions: any;
  constructor(private os: OptionsService) { }

  ngOnInit() {
    this.setNavOptionsandText();
  }

  setNavOptionsandText( ) {
    this.navOptions = this.os.getAllOptions();
    this.componentText = this.os.getAllText();
    this.advancedOptions = this.os.getAdvancedSettingsOptions();
  }

}
